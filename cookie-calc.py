print('''
 .d8888b.   .d8888b.   .d8888b.  
d88P  Y88b d88P  Y88b d88P  Y88b 
888    888 888    888 888    888 
888        888        888        
888        888        888        
888    888 888    888 888    888 
Y88b  d88P Y88b  d88P Y88b  d88P 
 "Y8888P"   "Y8888P"   "Y8888P"  Cookie Clicker Calculator [v1]
''')


def Xcookies_in_Xseconds():
    cookies_per_seconds = int(input('cookies per second: '))
    redquired_number_of_cookies = int(input('required number of cookies: '))
    result = redquired_number_of_cookies / cookies_per_seconds
    print(f'''
------------------------------------
Cookies per seconds: {cookies_per_seconds} cookies
Cookies required: {redquired_number_of_cookies} cookies
------------------------------------
    ''')
    start_or_not = input('start? (Y/N) > ')
    if start_or_not == 'Y':
        print(str(redquired_number_of_cookies) +
              ' cookies will be collected in \033[1m' + str(result) + '\033[0m seconds')
    if start_or_not == 'y':
        print(str(redquired_number_of_cookies) +
              ' cookies will be collected in \033[1m' + str(result) + '\033[0m seconds')
    if start_or_not == 'n':
        print('canceled')
    if start_or_not == 'N':
        print('canceled')


def in_Xseconds_Xcookies():
    cookies_per_seconds = int(input('cookies per second: '))
    time_of_the_break = int(input('time of your break (in seconds): '))
    result = cookies_per_seconds * time_of_the_break
    print(f'''
------------------------------------
Cookies per seconds: {cookies_per_seconds} cookies
Time of the break: {time_of_the_break} seconds
------------------------------------
    ''')
    start_or_not = input('start? (Y/N) > ')
    if start_or_not == 'Y':
        print('\033[1m' + str(result) + '\033[0m cookies will be gained in ' +
              time_of_the_break + ' seconds')
    if start_or_not == 'y':
        print('\033[1m' + str(result) + '\033[0m cookies will be gained in ' +
              str(time_of_the_break) + ' seconds')
    if start_or_not == 'n':
        print('canceled')
    if start_or_not == 'N':
        print('canceled')


print('''
[1] Time calculator (X cookies in X seconds)
[2] Break calculator (in X seconds X cookies)
''')
selected_method = input('select a method: ')
if selected_method == '1':
    print('\n')
    Xcookies_in_Xseconds()
if selected_method == '2':
    print('\n')
    in_Xseconds_Xcookies()
